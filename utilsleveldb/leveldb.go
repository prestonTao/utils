package utilsleveldb

import (
	"github.com/hyahm/golog"
	"github.com/syndtr/goleveldb/leveldb"
	"sync"
	"web3_gui/utils"
)

type LevelDB struct {
	path                string
	db                  *leveldb.DB
	tr                  *leveldb.Transaction
	once                sync.Once
	leveldbIndexMapLock *sync.RWMutex         //
	leveldbIndexMap     map[string]*IndexLock //保存不同表的index
}

func CreateLevelDB(path string) (*LevelDB, error) {
	lldb := LevelDB{
		path:                path,
		once:                sync.Once{},
		leveldbIndexMapLock: new(sync.RWMutex),
		leveldbIndexMap:     make(map[string]*IndexLock),
	}
	err := lldb.InitDB()
	if err != nil {
		return nil, err
	}
	return &lldb, nil
}

// 链接leveldb
func (this *LevelDB) InitDB() (err error) {
	this.once.Do(func() {
		//没有db目录会自动创建
		this.db, err = leveldb.OpenFile(this.path, nil)
		if err != nil {
			return
		}
		return
	})
	return
}

/*
保存
*/
func (this *LevelDB) Save(key LeveldbKey, bs *[]byte) utils.ERROR {
	if ERR := checkValueSize(*bs); !ERR.CheckSuccess() {
		return ERR
	}
	err := this.db.Delete(key.key, nil)
	if err != nil {
		return utils.NewErrorSysSelf(err)
	}
	if bs == nil {
		err = this.db.Put(key.key, nil, nil)
	} else {
		err = this.db.Put(key.key, *bs, nil)
	}
	return utils.NewErrorSysSelf(err)
}

/*
保存
*/
func (this *LevelDB) SaveBatch(batch *leveldb.Batch) error {
	return this.db.Write(batch, nil)
}

/*
保存 事务处理
*/
func (this *LevelDB) Save_Transaction(key LeveldbKey, bs *[]byte) utils.ERROR {
	if ERR := checkValueSize(*bs); !ERR.CheckSuccess() {
		return ERR
	}
	err := this.tr.Delete(key.key, nil)
	if err != nil {
		return utils.NewErrorSysSelf(err)
	}
	if bs == nil {
		err = this.tr.Put(key.key, nil, nil)
	} else {
		err = this.tr.Put(key.key, *bs, nil)
	}
	return utils.NewErrorSysSelf(err)
}

/*
保存多条数据
*/
func (this *LevelDB) SaveMore(kvps ...KVPair) error {
	err := this.OpenTransaction()
	if err != nil {
		return err
	}
	err = this.SaveMore_TransacTion(kvps...)
	if err != nil {
		this.Discard()
		return err
	}
	err = this.Commit()
	if err != nil {
		this.Discard()
		return err
	}
	return nil
}

/*
保存多条数据 事务处理
*/
func (this *LevelDB) SaveMore_TransacTion(kvps ...KVPair) error {
	var err error
	for _, one := range kvps {
		if one.IsAddOrDel {
			//levedb保存相同的key，原来的key保存的数据不会删除，因此保存之前先删除原来的数据
			err = this.tr.Delete(one.Key, nil)
			if err != nil {
				return err
			}
			err = this.tr.Put(one.Key, one.Value, nil)
		} else {
			err = this.tr.Delete(one.Key, nil)
		}
		if err != nil {
			return err
		}
	}
	return nil
}

/*
删除
*/
func (this *LevelDB) Remove(key LeveldbKey) error {
	return this.db.Delete(key.key, nil)
}

/*
删除 事务处理
*/
func (this *LevelDB) Remove_Transaction(key LeveldbKey) error {
	return this.tr.Delete(key.key, nil)
}

/*
删除多个操作
*/
func (this *LevelDB) RemoveMore(kvps ...KVPair) error {
	return this.SaveMore(kvps...)
}

/*
删除多个操作 事务处理
*/
func (this *LevelDB) RemoveMore_Transaction(kvps ...KVPair) error {
	return this.SaveMore_TransacTion(kvps...)
}

/*
查找
*/
func (this *LevelDB) Find(key LeveldbKey) (*DBItem, error) {
	items, err := this.FindMore(key)
	if err != nil {
		return nil, err
	}
	if items != nil && len(items) > 0 {
		return &items[0], nil
	}
	return nil, nil

}

/*
查找多条记录
*/
func (this *LevelDB) FindMore(keys ...LeveldbKey) ([]DBItem, error) {
	items := make([]DBItem, 0, len(keys))
	sn, err := this.db.GetSnapshot()
	if err != nil {
		return nil, err
	}
	for _, one := range keys {
		value, err := sn.Get(one.key, nil)
		if err != nil {
			if err == leveldb.ErrNotFound {
				return nil, nil
			}
			sn.Release()
			return nil, err
		}
		item := DBItem{
			Key:   one,
			Value: value,
		}
		items = append(items, item)
	}
	sn.Release()
	return items, nil
}

var once = new(sync.Once)

/*
打印所有key
*/
func (this *LevelDB) PrintAll() ([][]byte, error) {
	once.Do(func() {
		golog.InitLogger("logs", 0, true)
	})
	iter := this.db.NewIterator(nil, nil)
	for iter.Next() {
		value := iter.Value()
		if len(value) > 100 {
			golog.Info("key:%+v valueLength:%d %+v", iter.Key(), len(value), value[:100])
		} else {
			golog.Info("key:%+v valueLength:%d %+v", iter.Key(), len(value), value)
		}
		// fmt.Println("key", hex.EncodeToString(iter.Key()), "value", hex.EncodeToString(iter.Value()))
		//fmt.Println("key", iter.Key(), "value", iter.Value())
	}
	golog.Info("end---------------------")
	iter.Release()
	err := iter.Error()
	return nil, err
}

/*
检查key是否存在
@return    bool    true:存在;false:不存在;
*/
func (this *LevelDB) Has(key LeveldbKey) (bool, error) {
	sn, err := this.db.GetSnapshot()
	if err != nil {
		return false, err
	}
	has, err := sn.Has(key.key, nil)
	sn.Release()
	return has, err
}

/*
获取数据库连接
*/
func (this *LevelDB) GetDB() *leveldb.DB {
	return this.db
}

/*
关闭leveldb连接
*/
func (this *LevelDB) Close() {
	this.db.Close()
}

/*
开启事务
*/
func (this *LevelDB) OpenTransaction() error {
	var err error
	this.tr, err = this.db.OpenTransaction()
	return err
}

/*
提交事务
*/
func (this *LevelDB) Commit() error {
	err := this.tr.Commit()
	if err != nil {
		this.tr.Discard()
		return err
	}
	return nil
}

/*
回滚事务
*/
func (this *LevelDB) Discard() {
	this.tr.Discard()
}

/*
获取数据库所在磁盘路径
*/
func (this *LevelDB) GetPath() string {
	return this.path
}
