/*
指数退避算法
*/
package utils

import (
	"context"
	"sync/atomic"
	"time"
)

type BackoffTimer struct {
	botc *BackoffTimerChan
}

/*
等待时间
@return    time.Duration    等待的时间
*/
func (this *BackoffTimer) Wait() time.Duration {
	return this.botc.Wait(context.Background())
}

/*
重置
间隔时间从头开始
*/
func (this *BackoffTimer) Reset() {
	this.botc.Reset()
}

/*
立即释放暂停的程序
*/
func (this *BackoffTimer) Release() {
	this.botc.Release()
}

/*
间隔n秒后发送一个信号
*/
func NewBackoffTimer(n ...int64) *BackoffTimer {
	ns := make([]time.Duration, 0, len(n))
	for _, one := range n {
		ns = append(ns, time.Second*time.Duration(one))
	}
	bt := BackoffTimer{botc: NewBackoffTimerChan(ns...)}
	return &bt
}

type BackoffTimerChan struct {
	interval []time.Duration //退避间隔时间，单位：秒
	index    int32           //当前间隔时间下标
	release  chan bool       //
}

/*
等待时间
@return    time.Duration    等待的时间
*/
func (this *BackoffTimerChan) Wait(c context.Context) time.Duration {
	n := this.interval[this.index]
	if int(atomic.LoadInt32(&this.index)+1) < len(this.interval) {
		atomic.AddInt32(&this.index, 1)
	}
	timer := time.NewTimer(n)
	select {
	case <-timer.C:
	case <-this.release:
		timer.Stop()
	case <-c.Done():
		timer.Stop()
		return 0
	}
	// time.Sleep(time.Second * time.Duration(n))
	return n
}

/*
重置
间隔时间从头开始
*/
func (this *BackoffTimerChan) Reset() {
	atomic.StoreInt32(&this.index, 0)
}

/*
立即释放暂停的程序
*/
func (this *BackoffTimerChan) Release() {
	select {
	case this.release <- false:
	default:
	}
}

/*
间隔n秒后发送一个信号
*/
func NewBackoffTimerChan(n ...time.Duration) *BackoffTimerChan {
	return &BackoffTimerChan{
		interval: n,
		release:  make(chan bool, 1),
	}
}
