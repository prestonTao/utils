package utilsleveldb

import (
	"gitee.com/prestonTao/utils"
	"sync"
)

var dbKeyRegisterMap *sync.Map = new(sync.Map) //key:uint64=;value:=;

/*
注册一个数据库ID，并判断是否有重复
@return    bool    是否成功
*/
func RegisterDbKey(id []byte) bool {
	_, ok := dbKeyRegisterMap.LoadOrStore(utils.Bytes2string(id), nil)
	if ok {
		utils.Log.Error().Bytes("重复注册的数据库ID", id)
	}
	return !ok
}
