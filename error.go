package utils

import (
	"gitee.com/prestonTao/utils/protos/go_protos"
	"github.com/gogo/protobuf/proto"
)

const (
	ERROR_CODE_success             = 60000 //成功
	ERROR_CODE_system_error_remote = 60001 //系统错误--远程节点
	ERROR_CODE_system_error_self   = 60002 //系统错误--自己节点
)

type ERROR struct {
	Code uint64 `json:"c"` //业务错误编号
	Msg  string `json:"m"` //错误信息
}

func (this *ERROR) Proto() (*[]byte, error) {
	bhp := go_protos.ERROR{
		Code: this.Code,
		Msg:  this.Msg,
	}
	bs, err := bhp.Marshal()
	return &bs, err
}

func ParseERROR(bs []byte) (*ERROR, error) {
	if bs == nil {
		return nil, nil
	}
	sdi := new(go_protos.ERROR)
	err := proto.Unmarshal(bs, sdi)
	if err != nil {
		return nil, err
	}
	nr := ERROR{
		Code: sdi.Code,
		Msg:  sdi.Msg,
	}
	return &nr, nil
}

/*
检查这个错误类型是否是成功
*/
func (this *ERROR) CheckSuccess() bool {
	if this.Code == ERROR_CODE_success {
		return true
	}
	return false
}

/*
检查这个错误类型是否是成功
*/
func (this *ERROR) String() string {
	bs, err := json.Marshal(this)
	if err != nil {
		return err.Error()
	}
	return string(bs)
}

/*
创建一个系统错误
*/
func NewErrorSysSelf(err error) ERROR {
	if err == nil {
		return ERROR{
			Code: ERROR_CODE_success,
		}
	}
	return ERROR{
		Code: ERROR_CODE_system_error_self,
		Msg:  err.Error(),
	}
}

/*
创建一个远端节点的系统错误
*/
func NewErrorSysRemote(msg string) ERROR {
	return ERROR{
		Code: ERROR_CODE_system_error_remote,
		Msg:  msg,
	}
}

/*
创建一个业务错误
*/
func NewErrorBus(code uint64, msg string) ERROR {
	return ERROR{
		Code: code,
		Msg:  msg,
	}
}

/*
创建一个返回成功
*/
func NewErrorSuccess() ERROR {
	return ERROR{
		Code: ERROR_CODE_success,
	}
}
