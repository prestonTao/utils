/*
全局日志记录 使用"github.com/rs/zerolog"
TRACE(-1)：用于跟踪代码执行路径
DEBUG(0): 对故障排除有用的信息
INFO(1): 描述应用程序正常运行的信息
WARNING(2): 对于需要的记录事件，以后可能需要检查
ERROR(3): 特定操作的错误信息
FATAL(4): 应用程序无法恢复的严重错误。os.Exit(1) 在记录消息后调用
PANIC(5): 与 FATAL 类似，但只是名字改成了 PANIC()
*/

package utils

import (
	"github.com/rs/zerolog"
	"io"
	"os"
	"path/filepath"
)

var Log zerolog.Logger

func init() {
	//自定义预设字段名称
	zerolog.TimestampFieldName = "t" //时间
	zerolog.LevelFieldName = "l"     //级别
	zerolog.MessageFieldName = "m"   //消息
	zerolog.CallerFieldName = "c"    //文件行号
	zerolog.ErrorFieldName = "e"     //
	LogBuildDefaultConsole()
}

/*
默认日志配置，使用json格式，输出到控制台
*/
func LogBuildDefaultConsole() {
	//var output io.Writer = &zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: "2006-01-02 15:04:05.000"}
	//if os.Getenv("GO_ENV") != "development" {
	//	output = os.Stderr
	//}
	Log = zerolog.New(os.Stderr).
		//Level(zerolog.TraceLevel).
		With().
		Timestamp().
		Caller().
		//Int("pid", os.Getpid()).
		//Str("go_version", buildInfo.GoVersion).
		Logger()
}

/*
默认日志配置，使用json格式，输出到文件
*/
func LogBuildDefaultFile(filename string) error {
	//
	fileDir, err := filepath.Abs(filepath.Dir(filename))
	if err != nil {
		Log.Error().Str("e", err.Error())
		return err
	}
	Log.Info().Str("log file output path", filepath.Join(fileDir, filename)).Send()
	err = CheckCreateDir(fileDir)
	if err != nil {
		return err
	}
	file, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0664)
	if err != nil {
		return err
	}
	Log = zerolog.New(file).
		//Level(zerolog.TraceLevel).
		With().
		Timestamp().
		Caller().
		//Int("pid", os.Getpid()).
		//Str("go_version", buildInfo.GoVersion).
		Logger()
	return nil
}

/*
构建为ConsoleWriter格式输出
注意不要在生产环境中使用 ， ConsoleWriter 因为它会大大减慢日志记录的速度。它只是为了帮助在开发应用程序时使日志更易于阅读。您可以使用环境变量仅在开发中启用 ConsoleWriter 输出
*/
func LogBuildColorOutputConsole() {
	var output io.Writer = &zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: "2006-01-02 15:04:05.000"}
	//l.With().Timestamp().Caller().Logger()
	Log = zerolog.New(output).
		//Level(zerolog.TraceLevel).
		With().
		Timestamp().
		Caller().
		//Int("pid", os.Getpid()).
		//Str("go_version", buildInfo.GoVersion).
		Logger()
}

/*
构建为ConsoleWriter格式输出
注意不要在生产环境中使用 ， ConsoleWriter 因为它会大大减慢日志记录的速度。它只是为了帮助在开发应用程序时使日志更易于阅读。您可以使用环境变量仅在开发中启用 ConsoleWriter 输出
*/
func LogBuildColorOutputFile(filename string) error {
	//
	fileDir, err := filepath.Abs(filepath.Dir(filename))
	if err != nil {
		Log.Error().Str("e", err.Error())
		return err
	}
	Log.Info().Str("log file output path", filepath.Join(fileDir, filename)).Send()
	err = CheckCreateDir(fileDir)
	if err != nil {
		return err
	}
	file, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0664)
	if err != nil {
		return err
	}
	var output io.Writer = &zerolog.ConsoleWriter{Out: file, TimeFormat: "2006-01-02 15:04:05.000"}
	//l.With().Timestamp().Caller().Logger()
	Log = zerolog.New(output).
		//Level(zerolog.TraceLevel).
		With().
		Timestamp().
		Caller().
		//Int("pid", os.Getpid()).
		//Str("go_version", buildInfo.GoVersion).
		Logger()
	return nil
}
