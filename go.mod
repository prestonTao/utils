module gitee.com/prestonTao/utils

go 1.18

require (
	github.com/astaxie/beego v1.12.3
	github.com/gogo/protobuf v1.1.1
	github.com/golang/protobuf v1.4.2
	github.com/hyahm/golog v0.0.2
	github.com/json-iterator/go v1.1.12
	github.com/ledisdb/ledisdb v0.0.0-20200510135210-d35789ec47e6
	github.com/mr-tron/base58 v1.2.0
	github.com/syndtr/goleveldb v1.0.0
	golang.org/x/crypto v0.15.0
)

require (
	github.com/cupcake/rdb v0.0.0-20161107195141-43ba34106c76 // indirect
	github.com/edsrzf/mmap-go v0.0.0-20170320065105-0bce6a688712 // indirect
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pelletier/go-toml v1.2.0 // indirect
	github.com/rs/zerolog v1.33.0 // indirect
	github.com/shiena/ansicolor v0.0.0-20151119151921-a422bbe96644 // indirect
	github.com/siddontang/go v0.0.0-20170517070808-cb568a3e5cc0 // indirect
	github.com/siddontang/rdb v0.0.0-20150307021120-fc89ed2e418d // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sys v0.24.0 // indirect
	google.golang.org/protobuf v1.23.0 // indirect
)
